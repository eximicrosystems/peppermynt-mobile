//
//  RestaurantFavViewController.swift
//  Peppermynt_Restaurant
//
//  Created by Oscar Sevilla Garduño on 03/01/22.
//

import UIKit

class RestaurantFavViewController: UIViewController {

    @IBOutlet var menusButton: UIButton!
    @IBOutlet var lock: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.addComponents(vc: self)
        menusButton.layer.borderColor = #colorLiteral(red: 0.8290262818, green: 0.6027662158, blue: 0.6283075809, alpha: 1)
        menusButton.layer.borderWidth = 3
        // Do any additional setup after loading the view.
    }
    @IBAction func menuActionButton(_ sender: Any) {
        let lMenu = LateralMenu(frame: CGRect(x: 80.0, y: 0, width: 334.0, height: UIScreen.main.bounds.height))
        UIApplication.shared.keyWindow!.rootViewController?.view.addSubview(lMenu)
    }
}
