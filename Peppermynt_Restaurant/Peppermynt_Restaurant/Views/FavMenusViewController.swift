//
//  FavMenusViewController.swift
//  Peppermynt_Restaurant
//
//  Created by Oscar Sevilla Garduño on 03/01/22.
//

import UIKit

class FavMenusViewController: UIViewController {

    @IBOutlet var appetizers: UIView!
    @IBOutlet var mains: UIView!
    @IBOutlet var sides: UIView!
    @IBOutlet var salads: UIView!
    @IBOutlet var beverages: UIView!
    @IBOutlet var wine: UIView!
    @IBOutlet var menus: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.addComponentsAct(vc: self)
        detailComponents()
    }
    
    func detailComponents(){
        menus.layer.borderColor = #colorLiteral(red: 0.8290262818, green: 0.6027662158, blue: 0.6283075809, alpha: 1)
        menus.layer.borderWidth = 3
        UIApplication.detailLogo(v: appetizers)
        UIApplication.detailLogo(v: mains)
        UIApplication.detailLogo(v: sides)
        UIApplication.detailLogo(v: salads)
        UIApplication.detailLogo(v: beverages)
        UIApplication.detailLogo(v: wine)
        
        let placeOrder = PlaceOrderView(frame: CGRect(x: 30, y: 130, width: 323.0, height: 539.0))
        UIApplication.addElement(v: placeOrder)
    }
    @IBAction func menuActionButton(_ sender: Any) {
        let lMenu = LateralMenu(frame: CGRect(x: 80.0, y: 0, width: 334.0, height: UIScreen.main.bounds.height))
        UIApplication.addElement(v: lMenu)
    }
}
