//
//  HomeViewController.swift
//  Peppermynt_Restaurant
//
//  Created by Oscar Sevilla Garduño on 27/12/21.
//

import UIKit

class HomeViewController: UIViewController {

    @IBOutlet var topten: UIView!
    @IBOutlet var bestdishes: UIView!
    @IBOutlet var reviews: UIView!
    @IBOutlet var recipes: UIView!
    @IBOutlet var privatefeedback: UIView!
    @IBOutlet var cookbook: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.addComponents(vc: self)
        detailComponents()
    }
    
    func detailComponents(){
        UIApplication.detailLogo(v: topten)
        UIApplication.detailLogo(v: bestdishes)
        UIApplication.detailLogo(v: reviews)
        UIApplication.detailLogo(v: recipes)
        UIApplication.detailLogo(v: privatefeedback)
        UIApplication.detailLogo(v: cookbook)
    }
    
    @IBAction func toptenActionButton(_ sender: Any) {
        UIApplication.nextView(view: TopTenViewController())
    }
    @IBAction func bestdishesActionButton(_ sender: Any) {
        UIApplication.nextView(view: BestDishesViewController())
    }
    @IBAction func menuActionButton(_ sender: Any) {
        let lMenu = LateralMenu(frame: CGRect(x: 80.0, y: 0, width: 334.0, height: UIScreen.main.bounds.height))
        UIApplication.shared.keyWindow!.rootViewController?.view.addSubview(lMenu)
    }
    
}
