//
//  TopTenViewController.swift
//  Peppermynt_Restaurant
//
//  Created by Oscar Sevilla Garduño on 31/12/21.
//

import UIKit

class TopTenViewController: UIViewController {

    @IBOutlet var topten: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.addComponents(vc: self)
        UIApplication.detailLogo(v: topten)
    }
    @IBAction func menuActionButton(_ sender: Any) {
        let lMenu = LateralMenu(frame: CGRect(x: 80.0, y: 0, width: 334.0, height: UIScreen.main.bounds.height))
        UIApplication.shared.keyWindow!.rootViewController?.view.addSubview(lMenu)
    }
}
