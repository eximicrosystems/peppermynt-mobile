//
//  ViewController.swift
//  Peppermynt_Restaurant
//
//  Created by Oscar Sevilla Garduño on 27/12/21.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet var login: UIButton!
    @IBOutlet var signup: UIButton!
    @IBOutlet var mobileNumberView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.navigationController?.isNavigationBarHidden = true
        login.layer.borderWidth = 2
        login.layer.borderColor = #colorLiteral(red: 0.8290262818, green: 0.6027662158, blue: 0.6283075809, alpha: 1)
        signup.layer.borderWidth = 2
        signup.layer.borderColor = #colorLiteral(red: 0.8290262818, green: 0.6027662158, blue: 0.6283075809, alpha: 1)
    }
    @IBAction func loginButtonAction(_ sender: Any) {
        if mobileNumberView.isHidden {
            signup.isHidden = true
            mobileNumberView.isHidden = false
        }else {
            UIApplication.nextView(view: HomeViewController())
        }
        
    }
    @IBAction func closeButtonAction(_ sender: Any) {
        signup.isHidden = false
        mobileNumberView.isHidden = true
    }
    
}

extension UIApplication {
    class func getTopViewController(base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return getTopViewController(base: nav.visibleViewController)

        } else if let tab = base as? UITabBarController, let selected = tab.selectedViewController {
            return getTopViewController(base: selected)

        } else if let presented = base?.presentedViewController {
            return getTopViewController(base: presented)
        }
        return base
    }
    
    class func nextView(view: UIViewController){
        if let VC = UIApplication.getTopViewController() {
            VC.navigationController?.pushViewController(view, animated: false)
        }
    }
    
    class func addComponents(vc: UIViewController){
//        let navBar = NavBar(frame: CGRect(x: 0, y: 44, width: UIScreen.main.bounds.width, height: 50))
//        vc.view.addSubview(navBar)
        let tabBar = Tabbar(frame: CGRect(x: 0, y: UIScreen.main.bounds.height-104, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height))
        vc.view.addSubview(tabBar)
    }
    
    class func addComponentsAct(vc: UIViewController){
        let tabBar = Tabbar(frame: CGRect(x: 0, y: UIScreen.main.bounds.height-104, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height))
        tabBar.callServerAct.isHidden = false
        tabBar.notificationIcon.isHidden = false
        vc.view.addSubview(tabBar)
    }
    
    class func detailLogo(v: UIView){
        v.layer.cornerRadius = (v.layer.frame.height / 2)
        v.layer.borderColor = #colorLiteral(red: 0.8290262818, green: 0.6027662158, blue: 0.6283075809, alpha: 1)
        v.layer.borderWidth = 3
    }
    
    class func detailPopup(v: UIView){
        v.layer.cornerRadius = 20
        v.layer.borderColor = #colorLiteral(red: 0.8290262818, green: 0.6027662158, blue: 0.6283075809, alpha: 1)
        v.layer.borderWidth = 3
    }
    
    class func detailButton(b: UIButton){
        b.layer.borderColor = #colorLiteral(red: 0.8290262818, green: 0.6027662158, blue: 0.6283075809, alpha: 1)
        b.layer.borderWidth = 2
    }
    
    class func addElement(v: UIView){
        self.shared.keyWindow!.rootViewController?.view.addSubview(v)
    }
}
