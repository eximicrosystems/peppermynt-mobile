//
//  PlaceOrderView.swift
//  Peppermynt_Restaurant
//
//  Created by Oscar Sevilla Garduño on 05/01/22.
//

import UIKit

class PlaceOrderView: UIView {
    
    @IBOutlet var placeOrderButton: UIButton!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
        UIApplication.detailPopup(v: self)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    func commonInit(){
        let viewFromXib = Bundle.main.loadNibNamed("PlaceOrderView", owner: self, options: nil)![0] as! UIView
        viewFromXib.frame = self.bounds
        addSubview(viewFromXib)
    }
    
    func detailComponents(){
        UIApplication.detailButton(b: placeOrderButton)
    }
    
    @IBAction func placeOrderActionButton(_ sender: Any) {
        self.removeFromSuperview()
    }
}
