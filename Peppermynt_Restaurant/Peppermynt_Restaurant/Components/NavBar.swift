//
//  NavBar.swift
//  Peppermynt_Restaurant
//
//  Created by Oscar Sevilla Garduño on 31/12/21.
//

import UIKit

class NavBar: UIView {
    @IBOutlet var logo: UIView!
    @IBOutlet var title: UILabel!
    @IBOutlet var logoImage: UIImageView!
    
    //init(frame: CGRect, title: String, image: String) {
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
        UIApplication.detailLogo(v: logo)
//        self.title.text = title
//        self.logoImage.image = UIImage(named: image)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    func commonInit(){
        let viewFromXib = Bundle.main.loadNibNamed("Tabbar", owner: self, options: nil)![0] as! UIView
        viewFromXib.frame = self.bounds
        addSubview(viewFromXib)
    }
    @IBAction func menuButtonAction(_ sender: Any) {
        UIApplication.shared.keyWindow!.rootViewController?.view.addSubview(LateralMenu(frame: CGRect(x: 80.0, y: 0, width: 334.0, height: UIScreen.main.bounds.height)))
    }
}
