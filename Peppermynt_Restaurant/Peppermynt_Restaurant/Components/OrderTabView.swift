//
//  OrderTabView.swift
//  Peppermynt_Restaurant
//
//  Created by Oscar Sevilla Garduño on 04/01/22.
//

import UIKit

class OrderTabView: UIView {
    
    @IBOutlet var mainGlobe: UIView!
    @IBOutlet var one: UIView!
    @IBOutlet var two: UIView!
    @IBOutlet var three: UIView!
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    func commonInit(){
        let viewFromXib = Bundle.main.loadNibNamed("OrderTabView", owner: self, options: nil)![0] as! UIView
        viewFromXib.frame = self.bounds
        addSubview(viewFromXib)
        addDetails()
    }
    
    func addDetails(){
        border(v: mainGlobe)
        border(v: one)
        border(v: two)
        border(v: three)
    }
    
    func border(v: UIView){
        v.layer.borderColor = #colorLiteral(red: 0.8290262818, green: 0.6027662158, blue: 0.6283075809, alpha: 1)
        v.layer.borderWidth = 2
    }
    @IBAction func removeView(_ sender: Any) {
        self.removeFromSuperview()
    }
}
