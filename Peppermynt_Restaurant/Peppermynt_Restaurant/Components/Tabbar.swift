//
//  Tabbar.swift
//  Peppermynt
//
//  Created by Oscar Sevilla Garduño on 22/11/21.
//

import UIKit

class Tabbar: UIView {

    @IBOutlet var notificationIcon: UIView!
    @IBOutlet var notLabel: UILabel!
    @IBOutlet var callServerAct: UIView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    func commonInit(){
        let viewFromXib = Bundle.main.loadNibNamed("Tabbar", owner: self, options: nil)![0] as! UIView
        viewFromXib.frame = self.bounds
        addSubview(viewFromXib)
        UIApplication.detailLogo(v: notificationIcon)
        UIApplication.detailLogo(v: callServerAct)
    }
    @IBAction func homeButton(_ sender: Any) {
        UIApplication.nextView(view: HomeViewController())
    }
    @IBAction func shiftsButton(_ sender: Any) {
        UIApplication.nextView(view: RestaurantFavViewController())
    }
    @IBAction func help(_ sender: Any) {
        UIApplication.nextView(view: FavMenusViewController())
    }
    @IBAction func tab(_ sender: Any) {
        UIApplication.addElement(v: OrderTabView(frame: CGRect(x: 150.0, y: 500, width: 200.0, height: 245.0)))
    }
}
