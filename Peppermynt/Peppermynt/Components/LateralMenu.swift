//
//  LateralMenu.swift
//  Peppermynt
//
//  Created by Oscar Sevilla Garduño on 26/11/21.
//

import Foundation
import UIKit

class LateralMenu: UIView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    func commonInit(){
        let viewFromXib = Bundle.main.loadNibNamed("LateralMenu", owner: self, options: nil)![0] as! UIView
        viewFromXib.frame = self.bounds
        addSubview(viewFromXib)
    }
    @IBAction func homeButton(_ sender: Any) {
        if let topVC = UIApplication.getTopViewController() {
            topVC.navigationController?.pushViewController(NextShiftViewController(), animated: false)
        }
        self.removeFromSuperview()
    }
    @IBAction func myProfileButton(_ sender: Any) {
        if let topVC = UIApplication.getTopViewController() {
            topVC.navigationController?.pushViewController(ProfileViewController(), animated: false)
        }
        self.removeFromSuperview()
    }
    @IBAction func earningsButton(_ sender: Any) {
        if let topVC = UIApplication.getTopViewController() {
            topVC.navigationController?.pushViewController(EarningsViewController(), animated: false)
        }
        self.removeFromSuperview()
    }
    @IBAction func tipjarButton(_ sender: Any) {
        if let topVC = UIApplication.getTopViewController() {
            topVC.navigationController?.pushViewController(TipJarViewController(), animated: false)
        }
        self.removeFromSuperview()
    }
    @IBAction func shiftsButton(_ sender: Any) {
        if let topVC = UIApplication.getTopViewController() {
            topVC.navigationController?.pushViewController(ShiftsViewController(), animated: false)
        }
        self.removeFromSuperview()
    }
    @IBAction func messagesButton(_ sender: Any) {
        if let topVC = UIApplication.getTopViewController() {
            topVC.navigationController?.pushViewController(MessagesViewController(), animated: false)
        }
        self.removeFromSuperview()
    }
    @IBAction func closeMenuButton(_ sender: Any) {
        self.removeFromSuperview()
    }
}
