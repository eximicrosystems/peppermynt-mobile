//
//  Tabbar.swift
//  Peppermynt
//
//  Created by Oscar Sevilla Garduño on 22/11/21.
//

import UIKit

class Tabbar: UIView {

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    func commonInit(){
        let viewFromXib = Bundle.main.loadNibNamed("Tabbar", owner: self, options: nil)![0] as! UIView
        viewFromXib.frame = self.bounds
        addSubview(viewFromXib)
    }
    @IBAction func homeButton(_ sender: Any) {
        if let topVC = UIApplication.getTopViewController() {
            topVC.navigationController?.pushViewController(NextShiftViewController(), animated: false)
        }
    }
    @IBAction func shiftsButton(_ sender: Any) {
        if let topVC = UIApplication.getTopViewController() {
            topVC.navigationController?.pushViewController(ShiftsViewController(), animated: false)
        }
    }
    @IBAction func help(_ sender: Any) {
        if let topVC = UIApplication.getTopViewController() {
            topVC.navigationController?.pushViewController(HelpNeededViewController(), animated: false)
        }
    }
    @IBAction func tab(_ sender: Any) {
        if let topVC = UIApplication.getTopViewController() {
            topVC.navigationController?.pushViewController(TabsViewController(), animated: false)
        }
    }
}
