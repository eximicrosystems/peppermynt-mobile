//
//  ViewController.swift
//  Peppermynt
//
//  Created by Oscar Sevilla Garduño on 22/11/21.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet var login: UIButton!
    @IBOutlet var email: UITextField!
    @IBOutlet var password: UITextField!
    @IBOutlet var loginView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.navigationController?.isNavigationBarHidden = true
        login.layer.borderWidth = 3
        login.layer.borderColor = #colorLiteral(red: 0.8290262818, green: 0.6027662158, blue: 0.6283075809, alpha: 1)
        email.layer.borderWidth = 2
        email.layer.borderColor = #colorLiteral(red: 0.8290262818, green: 0.6027662158, blue: 0.6283075809, alpha: 1)
        password.layer.borderWidth = 2
        password.layer.borderColor = #colorLiteral(red: 0.8290262818, green: 0.6027662158, blue: 0.6283075809, alpha: 1)
    }

    @IBAction func loginButton(_ sender: Any) {
//        loginView.isHidden = false
//        login.isHidden = true
        if let topVC = UIApplication.getTopViewController() {
            topVC.navigationController?.pushViewController(NextShiftViewController(), animated: false)
        }
    }
    
    @IBAction func closeButton(_ sender: Any) {
        loginView.isHidden = true
        login.isHidden = false
    }
}

extension UIApplication {
    class func getTopViewController(base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return getTopViewController(base: nav.visibleViewController)

        } else if let tab = base as? UITabBarController, let selected = tab.selectedViewController {
            return getTopViewController(base: selected)

        } else if let presented = base?.presentedViewController {
            return getTopViewController(base: presented)
        }
        return base
    }
}

