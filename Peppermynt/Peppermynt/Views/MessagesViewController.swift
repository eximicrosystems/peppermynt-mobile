//
//  MessagesViewController.swift
//  Peppermynt
//
//  Created by Oscar Sevilla Garduño on 25/11/21.
//

import UIKit

class MessagesViewController: UIViewController {

    @IBOutlet var messageLogo: UIView!
    @IBOutlet var textMessage: UITextField!
    @IBOutlet var clientMessage: UIView!
    @IBOutlet var restaurantMessage: UIView!
    @IBOutlet var threadView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addComponents()
        messageLogo.layer.borderWidth = 2
        messageLogo.layer.borderColor = #colorLiteral(red: 0.8290262818, green: 0.6027662158, blue: 0.6283075809, alpha: 1)
        textMessage.layer.borderWidth = 1
        textMessage.layer.borderColor = #colorLiteral(red: 0.8290262818, green: 0.6027662158, blue: 0.6283075809, alpha: 1)
        clientMessage.layer.borderColor = #colorLiteral(red: 0.8290262818, green: 0.6027662158, blue: 0.6283075809, alpha: 1)
        restaurantMessage.layer.borderColor = #colorLiteral(red: 0.8290262818, green: 0.6027662158, blue: 0.6283075809, alpha: 1)
    }
    func addComponents() {
        let tabBar = Tabbar(frame: CGRect(x: 0, y: UIScreen.main.bounds.height-104, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height))
        self.view.addSubview(tabBar)
    }
    @IBAction func menuButton(_ sender: Any) {
        let lMenu = LateralMenu(frame: CGRect(x: 80.0, y: 0, width: 334.0, height: UIScreen.main.bounds.height))
        UIApplication.shared.keyWindow!.rootViewController?.view.addSubview(lMenu)
    }
    @IBAction func clearThreadButton(_ sender: Any) {
        threadView.isHidden = true
    }
}
