//
//  LoginViewController.swift
//  Peppermynt
//
//  Created by Oscar Sevilla Garduño on 22/11/21.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet var email: UITextField!
    @IBOutlet var password: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        addComponents()
        email.layer.borderWidth = 2
        email.layer.borderColor = #colorLiteral(red: 0.8290262818, green: 0.6027662158, blue: 0.6283075809, alpha: 1)
        password.layer.borderWidth = 2
        password.layer.borderColor = #colorLiteral(red: 0.8290262818, green: 0.6027662158, blue: 0.6283075809, alpha: 1)
    }
    
    func addComponents() {
        let tabBar = Tabbar(frame: CGRect(x: 0, y: UIScreen.main.bounds.height-104, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height))
        self.view.addSubview(tabBar)
    }
    
    @IBAction func closeButton(_ sender: Any) {
        if let topVC = UIApplication.getTopViewController() {
            topVC.navigationController?.pushViewController(ViewController(), animated: false)
        }
    }
    
}
