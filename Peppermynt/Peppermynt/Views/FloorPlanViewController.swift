//
//  FloorPlanViewController.swift
//  Peppermynt
//
//  Created by Oscar Sevilla Garduño on 07/12/21.
//

import UIKit

class FloorPlanViewController: UIViewController {

    @IBOutlet var lightButtonView: UIView!
    @IBOutlet var floorView: UIView!
    @IBOutlet var table1: UIView!
    @IBOutlet var table2: UIView!
    @IBOutlet var table3: UIView!
    @IBOutlet var table4: UIView!
    @IBOutlet var table5: UIView!
    @IBOutlet var table6: UIView!
    @IBOutlet var table7: UIView!
    @IBOutlet var table8: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addComponents()
    }

    func addComponents() {
        let tabBar = Tabbar(frame: CGRect(x: 0, y: UIScreen.main.bounds.height-104, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height))
        floorView.layer.borderColor = #colorLiteral(red: 0.8290262818, green: 0.6027662158, blue: 0.6283075809, alpha: 1)
        floorView.layer.borderWidth = 2
        table1.layer.borderColor = #colorLiteral(red: 0.8290262818, green: 0.6027662158, blue: 0.6283075809, alpha: 1)
        table1.layer.borderWidth = 2
        table2.layer.borderColor = #colorLiteral(red: 0.8290262818, green: 0.6027662158, blue: 0.6283075809, alpha: 1)
        table2.layer.borderWidth = 2
        table3.layer.borderColor = #colorLiteral(red: 0.8290262818, green: 0.6027662158, blue: 0.6283075809, alpha: 1)
        table3.layer.borderWidth = 2
        table4.layer.borderColor = #colorLiteral(red: 0.8290262818, green: 0.6027662158, blue: 0.6283075809, alpha: 1)
        table4.layer.borderWidth = 2
        table5.layer.borderColor = #colorLiteral(red: 0.8290262818, green: 0.6027662158, blue: 0.6283075809, alpha: 1)
        table5.layer.borderWidth = 2
        table6.layer.borderColor = #colorLiteral(red: 0.8290262818, green: 0.6027662158, blue: 0.6283075809, alpha: 1)
        table6.layer.borderWidth = 2
        table7.layer.borderColor = #colorLiteral(red: 0.8290262818, green: 0.6027662158, blue: 0.6283075809, alpha: 1)
        table7.layer.borderWidth = 2
        table8.layer.borderColor = #colorLiteral(red: 0.8290262818, green: 0.6027662158, blue: 0.6283075809, alpha: 1)
        table8.layer.borderWidth = 2
        self.view.addSubview(tabBar)
        lightButtonView.layer.borderColor = #colorLiteral(red: 0.8290262818, green: 0.6027662158, blue: 0.6283075809, alpha: 1)
        lightButtonView.layer.borderWidth = 2
        self.view.bringSubviewToFront(lightButtonView)
    }
}
