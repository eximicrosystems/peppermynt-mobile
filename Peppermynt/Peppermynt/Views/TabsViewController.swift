//
//  TabsViewController.swift
//  Peppermynt
//
//  Created by Oscar Sevilla Garduño on 02/12/21.
//

import UIKit

class TabsViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        addComponents()
    }

    func addComponents() {
        let tabBar = Tabbar(frame: CGRect(x: 0, y: UIScreen.main.bounds.height-104, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height))
        self.view.addSubview(tabBar)
    }
    
    @IBAction func TabDetailButton(_ sender: Any) {
        if let topVC = UIApplication.getTopViewController() {
            topVC.navigationController?.pushViewController(TabsDetailViewController(), animated: false)
        }
    }
}
