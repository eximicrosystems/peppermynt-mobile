//
//  NextShiftViewController.swift
//  Peppermynt
//
//  Created by Oscar Sevilla Garduño on 25/11/21.
//

import UIKit

class NextShiftViewController: UIViewController {
    
    @IBOutlet var restInfo: UIButton!
    @IBOutlet var messageButton: UIButton!
    @IBOutlet var clockOutButton: UIButton!
    @IBOutlet var clockInButton: UIButton!
    @IBOutlet var clockInImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addComponents()
    }
    
    func addComponents(){
        let tabBar = Tabbar(frame: CGRect(x: 0, y: UIScreen.main.bounds.height-104, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height))
        self.view.addSubview(tabBar)
        restInfo.layer.borderWidth = 3
        restInfo.layer.borderColor = #colorLiteral(red: 0.8290262818, green: 0.6027662158, blue: 0.6283075809, alpha: 1)
        messageButton.layer.borderWidth = 3
        messageButton.layer.borderColor = #colorLiteral(red: 0.8290262818, green: 0.6027662158, blue: 0.6283075809, alpha: 1)
        clockOutButton.layer.borderWidth = 3
        clockOutButton.layer.borderColor = #colorLiteral(red: 0.8290262818, green: 0.6027662158, blue: 0.6283075809, alpha: 1)
        clockInButton.layer.borderWidth = 3
        clockInButton.layer.borderColor = #colorLiteral(red: 0.8290262818, green: 0.6027662158, blue: 0.6283075809, alpha: 1)
    }

    @IBAction func menuButton(_ sender: Any) {
        let lMenu = LateralMenu(frame: CGRect(x: 80.0, y: 0, width: 334.0, height: UIScreen.main.bounds.height))
        UIApplication.shared.keyWindow!.rootViewController?.view.addSubview(lMenu)
    }
    @IBAction func restInfoButton(_ sender: Any) {
        if let topVC = UIApplication.getTopViewController() {
            topVC.navigationController?.pushViewController(RestaurantInfoViewController(), animated: false)
        }
    }
    @IBAction func messageButton(_ sender: Any) {
        if let topVC = UIApplication.getTopViewController() {
            topVC.navigationController?.pushViewController(MessagesViewController(), animated: false)
        }
    }
    @IBAction func clockInButton(_ sender: Any) {
        clockInButton.backgroundColor = #colorLiteral(red: 0.8290262818, green: 0.6027662158, blue: 0.6283075809, alpha: 1)
        clockInButton.isEnabled = false
        clockInImage.image = UIImage(named: "Clock-In-Icon-Mauve-01 1")
        self.view.bringSubviewToFront(clockInImage)
        print("isLocked")
    }
    @IBAction func clockOutButton(_ sender: Any) {
        let alert = UIAlertController(title: "Alert", message: "Do you want to clock out?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            switch action.style{
                case .default:
                    print("default")
                    if !self.clockInButton.isEnabled{
                        self.clockInButton.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                        self.clockInButton.isEnabled = true
                        self.clockInImage.image = UIImage(named: "Clock-In-Icon-Mauve-1")
                        print("unlocked")
                    }
                case .cancel:
                print("cancel")
                
                case .destructive:
                print("destructive")
                
            }
        }))
        self.present(alert, animated: true, completion: nil)
    }
}
