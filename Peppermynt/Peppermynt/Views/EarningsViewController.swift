//
//  EarningsViewController.swift
//  Peppermynt
//
//  Created by Oscar Sevilla Garduño on 26/11/21.
//

import UIKit

class EarningsViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        addComponents()
        // Do any additional setup after loading the view.
    }
    func addComponents() {
        let tabBar = Tabbar(frame: CGRect(x: 0, y: UIScreen.main.bounds.height-104, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height))
        self.view.addSubview(tabBar)
    }
    @IBAction func menuButton(_ sender: Any) {
        let lMenu = LateralMenu(frame: CGRect(x: 80.0, y: 0, width: 334.0, height: UIScreen.main.bounds.height))
        UIApplication.shared.keyWindow!.rootViewController?.view.addSubview(lMenu)
    }
}
