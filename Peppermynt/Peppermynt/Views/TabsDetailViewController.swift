//
//  TabsDetailViewController.swift
//  Peppermynt
//
//  Created by Oscar Sevilla Garduño on 06/12/21.
//

import UIKit

class TabsDetailViewController: UIViewController {

    @IBOutlet var receipt: UIView!
    @IBOutlet var discountView: UIView!
    @IBOutlet var discount: UITextField!
    @IBOutlet var addDiscountView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addComponents()
    }
    
    func addComponents() {
        let tabBar = Tabbar(frame: CGRect(x: 0, y: UIScreen.main.bounds.height-104, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height))
        receipt.layer.borderColor = #colorLiteral(red: 0.8290262818, green: 0.6027662158, blue: 0.6283075809, alpha: 1)
        receipt.layer.borderWidth = 2
        discount.layer.borderColor = #colorLiteral(red: 0.8290262818, green: 0.6027662158, blue: 0.6283075809, alpha: 1)
        discount.layer.borderWidth = 2
        self.view.addSubview(tabBar)
    }
    
    @IBAction func editButton(_ sender: Any) {
        discountView.isHidden = false
    }
    @IBAction func saveButton(_ sender: Any) {
        discountView.isHidden = true
    }
    @IBAction func discountButton(_ sender: Any) {
        addDiscountView.isHidden = false
    }
    @IBAction func addDiscountButton(_ sender: Any) {
        addDiscountView.isHidden = true
    }
}
