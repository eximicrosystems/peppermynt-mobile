//
//  HelpNeededViewController.swift
//  Peppermynt
//
//  Created by Oscar Sevilla Garduño on 02/12/21.
//

import UIKit

class HelpNeededViewController: UIViewController {

    @IBOutlet var lightButtonView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addComponents()
    }
    
    func addComponents() {
        let tabBar = Tabbar(frame: CGRect(x: 0, y: UIScreen.main.bounds.height-104, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height))
        self.view.addSubview(tabBar)
        lightButtonView.layer.borderColor = #colorLiteral(red: 0.8290262818, green: 0.6027662158, blue: 0.6283075809, alpha: 1)
        lightButtonView.layer.borderWidth = 2
        self.view.bringSubviewToFront(lightButtonView)
    }
    @IBAction func tableViewButton(_ sender: Any) {
        if let topVC = UIApplication.getTopViewController() {
            topVC.navigationController?.pushViewController(FloorPlanViewController(), animated: false)
        }
    }
}
